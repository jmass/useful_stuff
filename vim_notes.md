# Vim Notes

---

## Macros
In vim, you can record macros by pressing ``q`` followed by a register (i.e. for instance another letter, often ``q`` again).

To end recording the macro, press ``q`` again. 

You can also define macros in your .vimrc (as seen below unter *Snippets*).

You can replay the macros by pressing ``@`` followed by the register, e.g. type ``@q`` to replay the macro stored previously with ``qq``.

### Snippets
You can add macros to your .vimrc, e.g.
in ``~/.vimrc`` add:
```
:let @m="Gidef main():\<cr>    #...\<cr>\<cr>if __name__ == \"__main__\"\:\<cr>    main()\<cr>\<esc>"
:let @p="ggi#!/usr/bin/env python\<cr># -*- coding: utf-8 -*-\<cr>\<cr>\<esc>"
```
for some python boilerplate.

You can use this macro by typing ``@m`` or ``@p`` respectively in normal mode.

---

### Numbered list 
From: https://www.thegeekstuff.com/2009/01/vi-and-vim-macro-tutorial-how-to-record-and-play/

Insert 1. then record the following in register a: ESC yyp ENTER CTRL-a

Replay 98-times: 98@a

---

### Enclose with parenthesis (or other):
e.g. ``b c w ( ) ESC P`` for enclosing one word with parentheses.
or ``b c w " " ESC P `` to surround a word with quotation marks.

To record the commands as a macro, type:
``qqbcw""\<esc>Pq`` and replay while your cursor is on the word with ``@q``.

For surrounding highlighted text (visual mode; does not work with visual line mode) you could do ``xi()\<esc>P``.

A better way to achieve this may be adding 
```
xnoremap <leader>s xi()<Esc>P
```
to your .vimrc, though. That way you could use ``\s`` after selecting the text.	 (From https://superuser.com/questions/875095/adding-parenthesis-around-highlighted-text-in-vim )
